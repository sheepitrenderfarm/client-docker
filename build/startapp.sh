#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "$(dirname "$(readlink -f "$0")")"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

pass_bool_arg () {
    CMD_ARG=$1
    CMD_VAL=$2
    if [ "${CMD_VAL,,}" == "true" ]; then #${,,} lowercase everything
        COMMAND="$COMMAND $CMD_ARG"
    fi
}

pass_arg () {
    CMD_ARG=$1
    CMD_VAL=$2
    if [ -n "$CMD_VAL" ]; then
        COMMAND="$COMMAND $CMD_ARG $CMD_VAL"
    fi
}

# End of Function definitions, begin of MAIN

set +u

if [ "${NO_AUTOUPDATE,,}" = true ]; then
    if [ -e /sheepit/client/LATEST_VERSION ]; then
        LATEST_VERSION=$(cat /sheepit/client/LATEST_VERSION)
        echo "No-autoupdate mode, evaluating jar from LATEST_VERSION file"
    else
        echo "LATEST_VERSION file not found. Try running in DOWNLOAD_ONLY mode beforehand. Aborting!"
        exit 4
    fi
else
    URL=https://www.sheepit-renderfarm.com/media/applet/client-latest.php
    if [ "${BETA,,}" = true ]; then
        URL=https://www.sheepit-renderfarm.com/media/applet/client-beta.php
    fi
    echo "Checking for client updates..."
    LATEST_VERSION=$(curl --connect-timeout 20 --retry 4 --silent --show-error --head $URL | \
        grep -Po '(?i)content-disposition:.*filename="?(?-i)\Ksheepit-client-[\d\.]+\d')

    if [ -z "$LATEST_VERSION" ]; then
        # Empty LATEST_VERSION is a good indicator of a critical failure
        echo "!!! Failed parsing version information! Aborting !!!"
        echo "Possible causes and troubleshooting steps:"
        echo "1. Check for internet connectivity \(Routes, DNS, Proxy\)"
        echo "2. Check the status of SheepIt via the SheepIt website: https://www.sheepit-renderfarm.com/"
        echo "3. Open an issue on GitLab/Ask on the Discord if problems persists after 1. and 2."
        exit 2
    elif [ ! -e "$LATEST_VERSION".jar ]; then
        echo "Updating client to version $LATEST_VERSION..."
        rm -f sheepit-client*.jar
        # Download new client.
        curl --connect-timeout 20 --retry 4 https://www.sheepit-renderfarm.com/media/applet/client-latest.php -o "$LATEST_VERSION".jar
    else
        echo "No updates found"
    fi

    if [ "${DOWNLOAD_ONLY,,}" = true ]; then
        echo "$LATEST_VERSION" > /sheepit/client/LATEST_VERSION
        echo "Download-only mode, exiting"
        exit 0
    fi
fi

COMMAND="java -jar /sheepit/client/$LATEST_VERSION.jar"

# Command building stage
pass_bool_arg "--help" "$SHEEP_HELP"
pass_bool_arg "--headless" "$SHEEP_HEADLESS"
pass_bool_arg "--no-gpu" "$SHEEP_NO_GPU"
pass_bool_arg "--no-systray" "$SHEEP_NO_SYSTRAY"
pass_bool_arg "--show-gpu" "$SHEEP_SHOW_GPU"
pass_bool_arg "--verbose" "$SHEEP_VERBOSE"
pass_bool_arg "--version" "$SHEEP_VERSION"
pass_arg "-cache-dir" "$SHEEP_CACHE_DIR"
pass_arg "-compute-method" "$SHEEP_COMPUTE_METHOD"
pass_arg "-config" "$SHEEP_CONFIG"
pass_arg "-cores" "$SHEEP_CORES"
pass_arg "-extras" "$SHEEP_EXTRA"
pass_arg "-gpu" "$SHEEP_GPU"
pass_arg "-hostname" "$SHEEP_HOSTNAME"
pass_arg "-login" "$SHEEP_LOGIN"
pass_arg "-memory" "$SHEEP_MEMORY"
pass_arg "-password" "$SHEEP_PASSWORD"
pass_arg "-priority" "$SHEEP_PRIORITY"
pass_arg "-proxy" "$SHEEP_PROXY"
pass_arg "-rendertime" "$SHEEP_RENDERTIME"
pass_arg "-request-time" "$SHEEP_REQUEST_TIME"
pass_arg "-server" "$SHEEP_SERVER"
pass_arg "-shared-zip" "$SHEEP_SHARED_ZIP"
pass_arg "-shutdown" "$SHEEP_SHUTDOWN"
pass_arg "-shutdown-mode" "$SHEEP_SHUTDOWN_MODE"
pass_arg "-theme" "$SHEEP_THEME"
pass_arg "-title" "$SHEEP_TITLE"
pass_arg "-ui" "$SHEEP_UI"

set -u

echo "Starting client with version $LATEST_VERSION"
${COMMAND}
