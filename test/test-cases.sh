#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/
export BASHOPTS # Make subshells inherit the shell options
#set -x # Print all executed commands
# Why not container-structure-test (cts)?
# It's another dependency in terms of network, support and knowledge
# It is unnecessarily restrictive and gets in the way more that it helps and additionally is very repetitive
# and if a shell script is necessary to work around some shortcomings and decrease duplicate lines/boilerplate anyway then cts can be ditched along the way
#
# Bash with explicit failure modes thanks to the strict mode will suffice for most if not all cases

caseecho () {
echo -e "|||=== TestCase $1 ================|||"

}

blender_test () {
    BLENDER_VERSION=$1
    BLENDER_URL=$2
    curl --connect-timeout 20 --retry 4 -sSo /sheepit/test/blender.zip "$BLENDER_URL"
    unzip -q /sheepit/test/blender.zip -d /sheepit/test/blender
    /sheepit/test/blender/rend.exe \
        -t 0 \
        --factory-startup \
        --disable-autoexec \
        -noaudio \
        -b /sheepit/test/compute-method.blend \
        -P "/sheepit/test/script-blender$BLENDER_VERSION.py" \
        --engine CYCLES \
        -o /sheepit/test/blender-frame_ \
        -f 0340 \
        -x 1  1>/dev/nul
    (set -x; test -e /sheepit/test/blender-frame_0340.png)
    rm -rf /sheepit/test/blender*
}

caseecho "Beta Jar Download"
(set -x; export BETA=true; export DOWNLOAD_ONLY=true; /sheepit/client/initapp.sh; test -e "/sheepit/client/$(cat /sheepit/client/LATEST_VERSION).jar")
caseecho "Beta Jar Run Version Output"
(set -x; export BETA=true; export NO_AUTOUPDATE=true; export SHEEP_VERSION=true; /sheepit/client/initapp.sh)
caseecho "Latest Jar Download"
(set -x; export DOWNLOAD_ONLY=true; /sheepit/client/initapp.sh; test -e "/sheepit/client/$(cat /sheepit/client/LATEST_VERSION).jar")
caseecho "Latest Jar Run Version Output"
(set -x; export NO_AUTOUPDATE=true; export SHEEP_VERSION=true; /sheepit/client/initapp.sh)
caseecho "Blender Run Version 3.6.4"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.6.4_linux_64bit.zip
caseecho "Blender Run Version 3.5.1"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.5.1_linux_64bit.zip
caseecho "Blender Run Version 3.4.1"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.4.1_linux_64bit.zip
caseecho "Blender Run Version 3.3.5"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.3.5_linux_64bit.zip
caseecho "Blender Run Version 3.2.2"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.2.2_linux_64bit.zip
caseecho "Blender Run Version 3.1.0"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.1.0_linux_64bit.zip
caseecho "Blender Run Version 3.0.1"
blender_test 3 https://sheepit-main-weur-binaries.sheepit-r2-binaries.win/blender3.0.1_linux_64bit.zip

echo "|||======================|||"
echo "|||  All Tests passed!!  |||"
echo "|||======================|||"
exit 0
